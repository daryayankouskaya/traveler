import { MaterialModule } from '../../shared/modules';
import { HeaderComponent } from './header.component';

export const HeaderTestingModule = {
  declarations: [HeaderComponent],
  imports: [MaterialModule],
  exports: [HeaderComponent],
};
