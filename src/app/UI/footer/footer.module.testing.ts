import { SharedModule } from '../../shared';
import { MaterialModule } from '../../shared/modules';
import { FooterComponent } from './footer.component';

export const FooterTestingModule = {
  declarations: [FooterComponent],
  imports: [SharedModule, MaterialModule],
  exports: [FooterComponent],
};
