import { NgModule } from '@angular/core';

import { SharedModule } from '../../shared';
import { MaterialModule } from '../../shared/modules';
import { FooterComponent } from './footer.component';

@NgModule({
  declarations: [FooterComponent],
  imports: [SharedModule, MaterialModule],
  exports: [FooterComponent],
})
export class FooterModule {}
