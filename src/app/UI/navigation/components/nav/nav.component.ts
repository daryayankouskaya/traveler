import { Component, Input, OnInit } from '@angular/core';

import { NavItem } from '../../interfaces/nav-item.interface';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss'],
})
export class NavComponent implements OnInit {
  @Input() navItems: NavItem[];

  constructor() {}

  ngOnInit() {}
}
