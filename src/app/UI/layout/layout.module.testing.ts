import { RouterTestingModule } from '@angular/router/testing';

import { SharedModule } from '../../shared';
import { FooterModule } from '../footer/footer.module';
import { HeaderModule } from '../header/header.module';
import { LayoutComponent } from './layout.component';

export const LayoutTestingModule = {
  declarations: [LayoutComponent],
  imports: [HeaderModule, FooterModule, SharedModule, RouterTestingModule],
};
