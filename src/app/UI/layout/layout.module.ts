import { NgModule } from '@angular/core';

import { SharedModule } from '../../shared';
import { FooterModule } from '../footer/footer.module';
import { HeaderModule } from '../header/header.module';
import { LayoutComponent } from './layout.component';

@NgModule({
  declarations: [LayoutComponent],
  imports: [HeaderModule, FooterModule, SharedModule],
})
export class LayoutModule {}
