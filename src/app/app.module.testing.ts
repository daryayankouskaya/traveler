import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { NgxsModule } from '@ngxs/store';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WeatherModule } from './pages/weather/weather.module';
import { SharedModule } from './shared';
import { MaterialModule } from './shared/modules';
import { AppState } from './shared/state';
import { LayoutModule } from './ui/layout/layout.module';

export const AppTestingModule = {
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NgxsModule.forRoot([AppState]),
    LayoutModule,
    SharedModule,
    MaterialModule,
    WeatherModule,
    RouterTestingModule,
  ],
  providers: [],
};
