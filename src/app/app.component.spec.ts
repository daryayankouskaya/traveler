import { async, TestBed } from '@angular/core/testing';

import { AppComponent } from './app.component';
import { AppTestingModule } from './app.module.testing';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule(AppTestingModule).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });
});
