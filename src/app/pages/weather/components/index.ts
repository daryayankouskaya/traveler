import { CurrentWeatherComponent } from './current-weather';
import { FiveDayWeatherComponent } from './five-day-weather/';

export { CurrentWeatherComponent, FiveDayWeatherComponent };
export const WEATHER_COMPONENTS = [
  CurrentWeatherComponent,
  FiveDayWeatherComponent,
];
