import { Component, OnDestroy, OnInit } from '@angular/core';
import { environment } from '@env/environment';
import { Actions, ofActionDispatched, Select } from '@ngxs/store';
import { Observable, Subscription } from 'rxjs';

import { HandleError } from '../../../../shared/state';
import { Weather } from '../../models/weather.model';
import { WeatherState } from '../../state/weather.state';

@Component({
  selector: 'app-current-weather',
  templateUrl: './current-weather.component.html',
  styleUrls: ['./current-weather.component.scss'],
})
export class CurrentWeatherComponent implements OnInit, OnDestroy {
  @Select(WeatherState.getCurrentWeather) public currentWeather$: Observable<
    Weather
  >;

  subscriptions = new Subscription();

  currentWeather: Weather;
  iconUrl: string;
  errorMessage: string;
  isLoaded = false;

  private citySearch404 = 'nothing found';

  constructor(private actions$: Actions) {}

  ngOnInit() {
    this.subscriptions.add(
      this.currentWeather$.subscribe(weather => {
        if (!weather) {
          return;
        }
        this.isLoaded = false;
        this.errorMessage = null;
        this.currentWeather = weather;
        const iconCode = weather.weather[0].icon;
        this.iconUrl = `${environment.ICON_URL}/${iconCode}.png`;
      }),
    );

    this.subscriptions.add(
      this.actions$
        .pipe(ofActionDispatched(HandleError))
        .subscribe(({ payload }: HandleError) => {
          const { status, message } = payload;

          if (status && status === 404) {
            this.currentWeather = null;
            this.errorMessage = message ? message : this.citySearch404;
          }
        }),
    );
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }
}
