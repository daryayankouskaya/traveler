import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NgxsModule } from '@ngxs/store';
import { SharedModule } from 'src/app/shared';
import { AppState } from 'src/app/shared/state';

import { WeatherService } from '../../services/weather.service';
import { FiveDayWeatherComponent } from './five-day-weather.component';

describe('FiveDayWeatherComponent', () => {
  let component: FiveDayWeatherComponent;
  let fixture: ComponentFixture<FiveDayWeatherComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FiveDayWeatherComponent],
      imports: [NgxsModule.forRoot([AppState]), SharedModule],
      providers: [WeatherService],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FiveDayWeatherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
