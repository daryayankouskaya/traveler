import { Component, OnDestroy, OnInit } from '@angular/core';
import { environment } from '@env/environment';
import { Actions, ofActionDispatched, Select, Store } from '@ngxs/store';
import { Observable, Subscription } from 'rxjs';

import { HandleError } from '../../../../shared/state';
import { FiveDayForecastModel } from '../../models/five-day-forecast.model';
import { Weather } from '../../models/weather.model';
import { FetchFiveDayWeather } from '../../state/weather.actions';
import { WeatherState } from '../../state/weather.state';

@Component({
  selector: 'app-five-day-weather',
  templateUrl: './five-day-weather.component.html',
  styleUrls: ['./five-day-weather.component.scss'],
})
export class FiveDayWeatherComponent implements OnInit, OnDestroy {
  @Select(WeatherState.getFiveDayWeather) public fiveDayWeather$: Observable<
    any
  >;

  @Select(WeatherState.getCurrentWeather) public currentWeather$: Observable<
    Weather
  >;

  subscriptions = new Subscription();

  cityForecasts: FiveDayForecastModel[] = [];
  name: string;
  code: string;
  iconCode: string;
  iconUrl: string;
  errorMessage: string;
  isLoaded = false;

  private citySearch404 = 'nothing found';

  constructor(private store: Store, private actions$: Actions) {}

  ngOnInit() {
    this.subscriptions.add(
      this.fiveDayWeather$.subscribe(res => {
        if (res) {
          for (let index = 0; index < res.list.length; index++) {
            this.errorMessage = null;
            this.isLoaded = false;
            const {
              city: { name, country },
              list,
            } = res;

            const treeHoursWeather: FiveDayForecastModel = {
              name,
              country,
              dt_txt: new Date(list[index].dt_txt),
              forecastHour: new Date(list[index].dt_txt).getHours(),
              weather: {
                temp: list[index].main.temp,
                pressure: list[index].main.pressure,
                description: list[index].weather[0].description,
                icon: list[index].weather[0].icon,
                iconUrl: `${environment.ICON_URL}/${list[index].weather[0].icon}.png`,
              },
            };

            this.name = name;
            this.code = country;

            this.cityForecasts.push(treeHoursWeather);
          }
        }
      }),
    );
    this.subscriptions.add(
      this.currentWeather$.subscribe(weather => {
        if (weather) {
          this.isLoaded = true;
          this.cityForecasts = [];
          this.store.dispatch(
            new FetchFiveDayWeather(weather.name, weather.sys.country),
          );
        }
      }),
    );

    this.subscriptions.add(
      this.actions$
        .pipe(ofActionDispatched(HandleError))
        .subscribe(({ payload }: HandleError) => {
          const { status, message } = payload;

          if (status && status === 404) {
            this.cityForecasts = null;
            this.errorMessage = message ? message : this.citySearch404;
          }
        }),
    );
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }
}
