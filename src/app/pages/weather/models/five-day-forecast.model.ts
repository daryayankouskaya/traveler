export interface FiveDayForecastModel {
  name: string;
  country: string;
  dt_txt: any;
  forecastHour: number;
  weather: {
    temp: number;
    pressure: number;
    description: string;
    icon: string;
    iconUrl: string;
  };
}
