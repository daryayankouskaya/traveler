import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CurrentWeatherComponent, FiveDayWeatherComponent } from './components';
import { WeatherComponent } from './weather.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'current',
  },
  {
    path: '',
    component: WeatherComponent,
    children: [
      { path: 'current', component: CurrentWeatherComponent },
      { path: 'five-days', component: FiveDayWeatherComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WeatherRoutingModule {}
