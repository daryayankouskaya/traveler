import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';

import { weatherNav } from './nav/weather-nav';
import { FetchCurrentWeather } from './state/weather.actions';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.scss'],
})
export class WeatherComponent implements OnInit {
  public weatherNav = weatherNav;

  constructor(private store: Store) {}

  public ngOnInit(): void {}

  public onSearch(search: string): void {
    this.store.dispatch(new FetchCurrentWeather(search));
  }
}
