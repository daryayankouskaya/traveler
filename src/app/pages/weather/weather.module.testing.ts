import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { NgxsModule } from '@ngxs/store';

import { SharedModule } from '../../shared';
import { MaterialModule } from '../../shared/modules';
import { AppState } from '../../shared/state';
import { WEATHER_COMPONENTS } from './components';
import { WeatherComponent } from './weather.component';
import { WeatherRoutingModule } from './weather.routing.module';

export const WeatherTestingModule = {
  declarations: [WeatherComponent, ...WEATHER_COMPONENTS],
  imports: [
    WeatherRoutingModule,
    BrowserAnimationsModule,
    SharedModule,
    MaterialModule,
    RouterTestingModule,
    NgxsModule.forRoot([AppState]),
  ],
};
