import { NavItem } from '../../../ui/navigation';

export const weatherNav: NavItem[] = [
  {
    title: 'Current Weather',
    link: '/weather/current',
  },
  {
    title: '5 Day Weather',
    link: '/weather/five-days',
  },
];
