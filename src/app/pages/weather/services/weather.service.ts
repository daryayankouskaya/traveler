import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';

import { Weather } from '../models/weather.model';

@Injectable({ providedIn: 'root' })
export class WeatherService {
  constructor(private http: HttpClient) {}

  public getCurrentWeather(cityName: string) {
    return this.http.get<Weather>(
      // tslint:disable-next-line: max-line-length
      `${environment.WEATHER_API}/${environment.WEATHER_ENDPOINT}?q=${cityName}&units=metric&APPID=${environment.APP_ID}`,
    );
  }

  public getFiveDayWeather(cityName: string, countryCode: string) {
    return this.http.get(
      // tslint:disable-next-line: max-line-length
      `${environment.WEATHER_API}/${environment.FORECAST_ENDPOINT}?q=${cityName},${countryCode}&units=metric&APPID=${environment.APP_ID}`,
    );
  }
}
