import { NgModule } from '@angular/core';
import { NgxsModule } from '@ngxs/store';

import { SharedModule } from '../../shared';
import { MaterialModule } from '../../shared/modules';
import { NavigationModule } from '../../ui/navigation/navigation.module';
import { WEATHER_COMPONENTS } from './components';
import { WeatherState } from './state/weather.state';
import { WeatherComponent } from './weather.component';
import { WeatherRoutingModule } from './weather.routing.module';

@NgModule({
  declarations: [WeatherComponent, ...WEATHER_COMPONENTS],
  imports: [
    WeatherRoutingModule,
    SharedModule,
    MaterialModule,
    NavigationModule,
    NgxsModule.forFeature([WeatherState]),
  ],
})
export class WeatherModule {}
