import { HttpErrorResponse } from '@angular/common/http';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import { HandleError } from '../../../shared/state';
import { Weather } from '../models/weather.model';
import { WeatherService } from '../services/weather.service';
import { FetchCurrentWeather, FetchFiveDayWeather } from './weather.actions';
import { defaults, WeatherStateModel } from './weather.state.model';

@State<WeatherStateModel>({
  name: 'weather',
  defaults,
})
export class WeatherState {
  constructor(private weatherService: WeatherService) {}

  @Selector()
  static getCurrentWeather(state: WeatherStateModel): Weather {
    return state.currentWeather;
  }

  @Selector()
  static getFiveDayWeather(state: WeatherStateModel) {
    return state.fiveDayWeather;
  }

  @Action(FetchCurrentWeather)
  fetchCurrentWeather(
    ctx: StateContext<WeatherStateModel>,
    { payload }: FetchCurrentWeather,
  ) {
    return this.weatherService.getCurrentWeather(payload).pipe(
      tap(weather => {
        ctx.patchState({ currentWeather: weather });

        ctx.dispatch(
          new FetchFiveDayWeather(weather.name, weather.sys.country),
        );
      }),

      catchError((errorHttp: HttpErrorResponse) => {
        const { error } = errorHttp;
        ctx.dispatch(
          new HandleError({ status: errorHttp.status, message: error.message }),
        );

        return throwError(errorHttp);
      }),
    );
  }

  @Action(FetchFiveDayWeather)
  fetchFiveDayWeather(
    ctx: StateContext<WeatherStateModel>,
    { name, code }: FetchFiveDayWeather,
  ) {
    return this.weatherService.getFiveDayWeather(name, code).pipe(
      tap(weather => {
        ctx.patchState({
          fiveDayWeather: weather,
        });
      }),

      catchError((errorHttp: HttpErrorResponse) => {
        const { error } = errorHttp;
        ctx.dispatch(
          new HandleError({ status: errorHttp.status, message: error.message }),
        );

        return throwError(errorHttp);
      }),
    );
  }
}
