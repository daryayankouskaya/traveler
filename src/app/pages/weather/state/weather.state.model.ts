import { Weather } from '../models/weather.model';

export interface WeatherStateModel {
  currentWeather: Weather;
  fiveDayWeather: any;
}

export const defaults: WeatherStateModel = {
  currentWeather: null,
  fiveDayWeather: null,
};
