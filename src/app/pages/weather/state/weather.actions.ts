export class FetchCurrentWeather {
  static readonly type = '[Weather] Fetch Current Weather';
  constructor(public payload: string) {}
}

export class FetchFiveDayWeather {
  static readonly type = '[Weather] Fetch Five Day Weather';
  constructor(public name: string, public code: string) {}
}
