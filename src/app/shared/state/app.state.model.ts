export interface AppStateModel {
  currentCity: string;
  countryCode: string;
  coords: { latitude: number; longitude: number };
}

export const defaults: AppStateModel = {
  currentCity: null,
  countryCode: null,
  coords: {
    latitude: null,
    longitude: null,
  },
};
