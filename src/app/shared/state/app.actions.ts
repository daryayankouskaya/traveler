import { Coords } from '../models/coords.model';

export class GetCurrentCoords {
  static readonly type = '[App] Get Current Coords';
}

export class GetCurrentCity {
  static readonly type = '[App] Get Current City';

  constructor(public payload: Coords) {}
}

export class HandleError {
  static readonly type = '[App] Handle Error';
  constructor(public payload: any) {}
}
