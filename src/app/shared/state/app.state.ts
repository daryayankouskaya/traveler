import { Action, Selector, State, StateContext, Store } from '@ngxs/store';
import { tap } from 'rxjs/operators';
import {
  FetchCurrentWeather,
} from 'src/app/pages/weather/state/weather.actions';

import { GeolocationService } from '../services';
import { GetCurrentCity, GetCurrentCoords } from './app.actions';
import { AppStateModel, defaults } from './app.state.model';

@State<AppStateModel>({
  name: 'app',
  defaults,
})
export class AppState {
  constructor(
    private geolocationService: GeolocationService,
    private store: Store,
  ) {}

  @Selector()
  static getCurrentCity(state: AppStateModel): string {
    return state.currentCity;
  }

  @Selector()
  static getCountryCode(state: AppStateModel) {
    return state.countryCode;
  }

  @Action(GetCurrentCoords)
  fetchCurrentCoords(ctx: StateContext<AppStateModel>) {
    this.geolocationService.getCurrentPosition().then(res => {
      const { latitude, longitude } = res;

      ctx.patchState({
        coords: {
          latitude,
          longitude,
        },
      });

      ctx.dispatch(new GetCurrentCity({ latitude, longitude }));
    });
  }

  @Action(GetCurrentCity)
  fetchCurrentCity(
    ctx: StateContext<AppStateModel>,
    { payload }: GetCurrentCity,
  ) {
    return this.geolocationService.getCurrentCity(payload).pipe(
      tap(response => {
        const { name, sys } = response;

        ctx.patchState({
          currentCity: name,
          countryCode: sys.country,
        });
        this.store.dispatch(new FetchCurrentWeather(name));
      }),
    );
  }
}
