import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';

import { Coords } from '../models/coords.model';

@Injectable({
  providedIn: 'root',
})
export class GeolocationService {
  constructor(private http: HttpClient) {}

  public getCurrentPosition(): Promise<any> {
    if (!navigator.geolocation) {
      throw new Error('no geolocation');
    }

    return new Promise<{ latitude: number; longitude: number }>(resolve => {
      navigator.geolocation.getCurrentPosition(position => {
        resolve(position.coords);
      });
    });
  }

  public getCurrentCity(coords: Coords) {
    return this.http.get<any>(
      // tslint:disable-next-line: max-line-length
      `${environment.WEATHER_API}/${environment.WEATHER_ENDPOINT}?lat=${coords.latitude}&lon=${coords.longitude}&APPID=${environment.APP_ID}`,
    );
  }
}
