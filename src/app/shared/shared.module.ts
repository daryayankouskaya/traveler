import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import {
  ErrorMessageComponent,
  LoadingSpinnerComponent,
  SearchComponent,
} from './components';
import { MaterialModule } from './modules';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule,
    MaterialModule,
    FlexLayoutModule,
  ],
  exports: [
    CommonModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule,
    SearchComponent,
    FlexLayoutModule,
    ErrorMessageComponent,
    LoadingSpinnerComponent,
  ],
  declarations: [
    SearchComponent,
    ErrorMessageComponent,
    LoadingSpinnerComponent,
  ],
})
export class SharedModule {}
