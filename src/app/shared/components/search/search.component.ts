import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
})
export class SearchComponent implements OnInit {
  @Input() public searchPlaceholder = 'Search...';
  @Output() search = new EventEmitter<string>();

  public fc: FormControl;

  constructor(private fb: FormBuilder) {}

  public ngOnInit(): void {
    this.fc = this.fb.control('');
  }

  onSearch() {
    const search = this.fc.value;

    this.search.emit(search);
  }
}
