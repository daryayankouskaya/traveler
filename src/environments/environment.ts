// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  APP_ID: 'a94472ca5654e608a3cb54d35d10efc8',
  // APP_ID: '',
  WEATHER_API: 'http://api.openweathermap.org/data/2.5',
  WEATHER_ENDPOINT: 'weather',
  FORECAST_ENDPOINT: 'forecast',
  ICON_URL: 'http://openweathermap.org/img/w',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
