export const environment = {
  production: true,
  APP_ID: 'a94472ca5654e608a3cb54d35d10efc8',
  // APP_ID: '',
  WEATHER_API: 'http://api.openweathermap.org/data/2.5',
  WEATHER_ENDPOINT: 'weather',
  FORECAST_ENDPOINT: 'forecast',
  ICON_URL: 'http://openweathermap.org/img/w',
};
